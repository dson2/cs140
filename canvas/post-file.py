#!/usr/bin/env python

import requests
import argparse
import getpass
import json
import os
import sys
from os.path import expanduser

parser = argparse.ArgumentParser(description='Upload file to canvas')
parser.add_argument('filepath', type=str, help='a file to upload')
parser.add_argument(
    '-D', '--destination', type=str, default="script-uploads",
    help='the folder on canvas to upload to')

args = parser.parse_args()
upload_filename = os.path.basename(args.filepath)


class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


canvas = dotdict()
canvas.host = "utk.instructure.com"
try:
    with open(expanduser("~/.canvas-auth-token")) as authfile:
        canvas.token = authfile.readline().strip()
        print("Auth token is " + str(len(canvas.token)) + " characters long.")
except IOError:
    print("CANVAS API TOKEN CREATION")
    print("======")
    print("You can get one at https://utk.instructure.com/profile/settings#access_tokens_holder")
    print("Click 'New Access Token', purpose: 'cs140', then click 'Generate Token'")
    print("Copy and paste that token here, it should look something like this: ")
    print("####~ag5hap9e85hgajergaAGshs46hrsagaeg38h9ATHFHDyhg")
    print("======")
    print("Please enter the canvas API token, input not shown:")
    canvas.token = getpass.getpass(prompt='Enter Token: ')
    with open(expanduser("~/.canvas-auth-token"), 'w') as authfile:
        authfile.write(canvas.token)
    os.chmod(expanduser("~/.canvas-auth-token"), 0o700)
    print("Token stored to ~/.canvas-auth-token")
canvas.scheme = "https"
canvas.baseurl = canvas.scheme + "://" + canvas.host
canvas.fileapi = "/api/v1/users/self/files"
canvas.fileapiurl = canvas.baseurl + canvas.fileapi
canvas.uploaddest = args.destination

header_auth = {
    'Authorization': ("Bearer " + canvas.token),
}

responses = dotdict

preupload_data = {
    'name': upload_filename,
    'parent_folder_path': canvas.uploaddest,
    'on_duplicate': 'overwrite',
}

responses.preupload = requests.post(
    canvas.fileapiurl,
    data=preupload_data,
    headers=header_auth,
)

print("code PREUP: " + str(responses.preupload))
# print("DEBUG preup headers: " + str(responses.preupload.json()))

if responses.preupload.status_code == 401:
    print("Bad auth token. Please correct the token.")
    print("Token should be placed in ~/.canvas-auth-token")
    sys.exit(1)
elif responses.preupload.status_code != 200:
    print("Something went wrong...")
    print(str(responses.preupload.text))

# print("DEBUG: PREUP json: " + str(json.dumps(responses.preupload.json(), indent=2)))

try:
    uploadfile = open(expanduser(args.filepath), 'rb')
except IOError as err:
    print("Error opening file to be uploaded:")
    print(err)

upload_params = responses.preupload.json()[u'upload_params']

# print("DEBUG upload_params json: " + str(json.dumps(upload_params, indent=2)))

# open file instances
upload_files = {
    'file': uploadfile,
}

responses.upload = requests.post(
    url=responses.preupload.json()[u'upload_url'],
    data=upload_params,
    allow_redirects=False,
    files=upload_files,
)

print("code UPLOAD: " + str(responses.upload))
# print("DEBUG head UPLOAD: " + str(responses.upload.headers))
confirm_url = responses.upload.headers['Location']
print("UPLOAD confirm url: " + confirm_url)

if responses.upload.status_code not in [200, 301, 303]:
    print("Error during file upload:")
    print(str(responses.upload.text))
    sys.exit(2)

responses.postupload = requests.post(
    url=confirm_url,
    headers=header_auth,
)

print(responses.postupload)
if responses.postupload.status_code not in [200]:
    print("Error confirming file upload: ")
    # print("DEBUG: " + str(responses.postupload.text))
    sys.exit(3)

print("Upload complete, file info: ")
print(json.dumps(responses.postupload.json(), indent=2))
