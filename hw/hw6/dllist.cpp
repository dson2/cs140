#include <iomanip>
#include <iostream>

using namespace std;

template <typename LT>
class list {
    struct node {
        node(LT data);
        LT data = LT();
        node *next;
        node *prev;
    };

  public:
    list<LT>() {
        head = new node;
        N = 0;
    }
    ~list() { clear(); delete head; }
    bool empty() const {
        return N == 0;
    }
    int size()   const {
        return N;
    }
    void clear() { while (!empty()) erase(0); }
    void insert(int i, const LT & din) {
        node *p = new node(din);
        node *pp = findnode(i-1);
        p->next = pp->next;
        p->prev = pp;
        p->next->prev = p;
        p->prev->next = p;
        N++;
    };
    void erase(int i) {
        node *p = findnode(i);
        p->prev->next = p->next;
        p->next->prev = p->prev;
        delete p;
        N--;
    };
    void push_back(const LT &din) { insert(N, din); }
    void pop_back() { erase(N-1); }
    const int & back();
    LT & operator[](int i) { node *p = findnode(i); return p->data;}

  private:
    int N;
    node *head;
    node *findnode(int i) {
        if (i == -1) return head;
        node *p;
        // midpoint = N/2
        // if target is < N/2: forward is faster
        if (i < ( (N / 2) + 1 ) ) {
            p = head->next;
            while (i--) p = p->next;
        }
        else {
            // back search
            p = head->prev;
            int delta = N - i;
            while (delta--) p = p->prev;
        }
        return p;
    };
};

template <typename LT>
void printlist(const char *operation, list<LT> &v) {
        cout << setw(14) << operation
             << " s=" << v.size()
             << " : ";
        for (int i=0; i<v.size(); i++) cout << " " << v[i]; cout << "\n";
}

template <typename LT>
list<LT>::node::node(LT n_data) {
    data = n_data;
    prev = this;
    next = this;
}

