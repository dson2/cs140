#include <iostream>
#include <list>

using namespace std;

template <typename T>
class neq {
  public:
    neq(const T &n_target = T()) { target = n_target; }
    bool operator() (const T &v) const { return v != target; }
  private:
    T target;
};

template <typename iT, typename Function>
int count(iT begin, iT end, Function notEqual) {
    int foundcount = 0;

    for (iT iter=begin; iter != end; ++iter) {
        if (notEqual(*iter) ) ++foundcount;
        // not any more.
    }
    return foundcount;
}

int main(int argc, char *argv[]) {
    list<int> v;
    list<int>::iterator iv;
    int value, target;

    while (cin >> value)
        v.push_back(value);

    cin.clear();

    cout << "Set target ";
    cin >> target;

    int N = count(v.begin(), v.end(), neq<int>(target));

    cout << "Found " << N << " elements not equal to target\n";
}
