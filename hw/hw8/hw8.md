# Problem 1:

a: O(n^2)
b: O(1)
c: O(N)
d: O(NlogN)

# Problem 2:

a: O(1) static address get
b: O(N) searching through N items

# Problem 3:

a: O(N) worst case goes through all elements (gets last or doesn't find)
b: O(N) linear comparison of each element

# Problem 4:

The comparison grows exponentially, meaning as larger numbers are considered, less numbers are needed to be checked. The number needed to check is less than N, however each iteration still needs to check a number more commonly than log(N) matches the values. The exponential rate function grows faster than log(N), but stays well below the 45degree x=N.
