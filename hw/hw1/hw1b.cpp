
#include <iostream>

using namespace std;

int strlen(char* instring) {
    int stridx = 0;
    while (instring[stridx] != '\0') {
        stridx++;
    }
    return stridx;
}

int main(int argc, char* argv[]) {
    cout << "Num args = " << argc << endl;

    for (int i = 0; i < argc; i++) {
        cout << "argv[" << i << "] = ";

        cout << &argv[i];

        cout << " ";
        cout << argv[i];

        cout << " (strlen=";
        cout << strlen(argv[i]);
        cout << ")";

        cout << endl;
    }

}

