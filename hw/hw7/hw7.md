# Problem 1

A hash table with SLL collision avoidance.

Keys: integers  
Table size: 10  
Hash Mapping: `key % 10`

Insertions: 12, 44, 13, 67, 23, 90, 11, 25, 21, 45

(10 items) `hash key: values in list`
```
0: 90
1: 11, 21
2: 12
3: 13, 23
4: 44
5: 25, 45
6:
7: 67
8:
9:
```

# Problem 2

A hash table, quadratic probing. (H+1 modifier)

Hash: `key % 23` (implying table size 23)

Insertion of the following `item: first hash <collision hash 2>*... <final hash>`
```
12: 12
44: 21
13: 13
67: 21*, 22
23: 0
90: 21*, 22*, 23
11: 11
25: 2
21: 21*, 22*, 23*, 24
45: 22*, 23*, 24*, 25
```
