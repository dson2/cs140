#include <iostream>

using namespace std;

#define ARRAYSIZE 10

class stack {
  public:
    //stack();
    //~stack();

    bool empty() const { return N == 0; };
    int size() const { return N; };

    void push(int data);
    void pop();
    int top();

  private:
    int intstack[ARRAYSIZE];
    int N = 0;

};

int main(int argc, char *argv[])
{
    stack v;
    int value;
    cout << "enter values:\n";
    while (cin >> value) {
        v.push(value);
        cout << value << "\n";
    }
    cout << "--- LIFO\n";
    while (!v.empty()) {
        value = v.top();
        v.pop();
        cout << value << "\n";
    }
}

/* This either needs to throw an error when the array is full, or it could
 * handle the resize automatically.
 */
void stack::push(int data) {
    intstack[(ARRAYSIZE - N) - 1] = data;
    N++;
}

/* If array is empty this should throw some kind of exception, or maybe zero?
 */
int stack::top() {
    return intstack[ARRAYSIZE - N];
}

/* Should check to make sure that array is not already empty before popping.
 *
 * Perhaps should throw an error, or return zero. (And not decrement N to negative.)
 */
void stack::pop() {
    intstack[(ARRAYSIZE - N)] = 0;
    N--;
}

