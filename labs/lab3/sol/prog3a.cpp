#include <cstdlib>
#include <iostream>
#include <iomanip>

#include <list>

using namespace std;

bool verbose = false;

bool isprime(const int inputint) {
    if (verbose) cout << "Checking number: " << inputint << endl;

    if (inputint <= 1) return false;
    else if (inputint <= 3) return true;
    else if (inputint % 2 == 0 || inputint % 3 == 0) return false;
    else {
        // go through M to sqrt(N)
        int i = 5;
        while (i * i <= inputint) {
            if (inputint % i == 0) return false;
            else if (inputint % (i+2) == 0) return false;
            // cheap trick to increment 6 as the lcm of 2 and 3
            i = i + 6;
        }
    }
    return true;
}

int main(int argc, char *argv[])
{
    for (int i=1; i<argc; i++) {
      string option = argv[i];
      if (option.compare("-verbose") == 0) {
        verbose = true;
      } else 
        cout << "option " << argv[i] << " ignored\n";
    }

    
    cerr << "Is it prime? [Input an integer]" << endl;
    int input_number;
    while (cin >> input_number) {
        if (isprime(input_number)) cout << "yep" << endl;
        else { cout << "nope" << endl; }
    }

}

