#include <cstdlib>
#include <iostream>
#include <iomanip>

#include <algorithm>
#include <vector>

using namespace std;

bool verbose = false;

class isprime {
  private:

  public:
    vector<int> primes;
    //isprime();
    //~isprime();

    // raw math to check primality
    bool primality_test(const int inputint) {
        //if (verbose) cout << "Checking number: " << inputint << endl;

        if (inputint <= 1) return false;
        else if (inputint <= 3) return true;
        else if (inputint % 2 == 0 || inputint % 3 == 0) return false;
        else {
            // go through M to sqrt(N)
            int i = 5;
            while (i * i <= inputint) {
                if (inputint % i == 0) return false;
                else if (inputint % (i+2) == 0) return false;
                // cheap trick to increment 6 as the lcm of 2 and 3
                i = i + 6;
            }
        }
        return true;
    }
    // true if number is in primes list
    bool prime_in_cache(const int inputint) {
        return binary_search(primes.begin(), primes.end(), inputint);

        //return std::find(primes.begin(), primes.end(), inputint) != primes.end();
    }
    // caches new primes while testing them
    bool primality_check_autocache(const int inputint) {
        if (prime_in_cache(inputint)) {
            //if (verbose) cout << "Number is prime (cached): " << inputint << endl;
            return true;
        }
        else {
            bool is_a_prime = primality_test(inputint);
            if (is_a_prime) {
                //if (verbose) cout << "Caching new prime: " << inputint << endl;
                primes.push_back(inputint);
                // because I'm lazy and I'm not going to bother writing the code for inserting into the sorted location
                // we already have to run through the whole sequence once,
                sort(primes.begin(), primes.end());
                return true;
            }
            else return false;
        }
    }

    // default action when instance is called as function
    bool operator()(const int inputint) {
        return primality_check_autocache(inputint);
    }

};

// operator for vector<int>
ostream& operator<< (ostream& outstream, vector<int>& intvect) {
    outstream << "vector<int>[";
    for (int x = 0; x != intvect.size(); ++x) {
        outstream << intvect[x] << ",";
    }
    outstream << "]";
}

// operator for vector<bool>
ostream& operator<< (ostream& outstream, vector<bool>& intvect) {
    outstream << "vector<bool>[";
    for (int x = 0; x != intvect.size(); ++x) {
        outstream << intvect[x] << ",";
    }
    outstream << "]";
}

// operator for isprime class
ostream& operator<< (ostream& outstream, isprime& primecheckclass) {
    outstream << "isprime(){primes: " << primecheckclass.primes << "}";
}

bool randgen = false; // random generator switch
int randseed = 0;
int randnum = 10; // default num numbers to generate

int randint_100() {
    return rand() % 100 + 1;
}

int main(int argc, char *argv[])
{
    for (int i=1; i<argc; i++) {
      string option = argv[i];
      if (option.compare("-verbose") == 0) {
        verbose = true;
      }
      else if (option.compare("-seed=") == 0) {
        randgen = true;
        randseed = atoi(&argv[i][6]);
        srand(randseed);
      } else {
        randgen = true;
        randnum = atoi(argv[i]);
        if (randseed == 0) randseed = randnum; // instructions says use N as seed as well...
        srand(randseed);
      }
    }

    isprime primechk = isprime();
    
    if (randgen == false) { // normal cin operation
        cerr << "Is it prime? [Input an integer]" << endl;

        int input_number;
        while (cin >> input_number) {
            if (primechk(input_number)) cout << "yep" << endl;
            else { cout << "nope" << endl; }

            // utility functions for debugging
            switch (input_number) {
                // -2: show isprime instance
                case -2: cout << primechk << endl; break;
            }
        }
    }
    else if (randgen == true) { // generate random numbers
        vector<int> generated_ints (randnum);
        generate(generated_ints.begin(), generated_ints.end(), randint_100);
        if (verbose) cout << "Random ints: " << generated_ints << endl;

        vector<bool> generated_ints_map (randnum);
        transform(generated_ints.begin(), generated_ints.end(), generated_ints_map.begin(), primechk);

        if (verbose) cout << "Prime map of random ints: " << generated_ints_map << endl;

        cout << "Number of primes in generated ints: " << count(generated_ints_map.begin(), generated_ints_map.end(), true) << endl;

        if (verbose) cout << primechk << endl;
    }

}

