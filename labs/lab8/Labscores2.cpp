#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <numeric>

using namespace std;

#define DEBUG false

class name_t {
  public:
    name_t(string n_last, string n_first) {
        last = n_last;
        first = n_first;
    }
    bool operator<(const name_t &othername) const {
        if (stringrep().compare(othername.stringrep()) < 0 ) return true;
        else return false;
    }
    // ya know we should totally be overloading an ostream operator instead here...
    // then setw might work here as well
    void print_name(uint fill_width = 0) const {
        cout << stringrep();
        uint filledcharcount = stringrep().length() + 1;
        cout << " ";
        // +2 for minimum number of dots
        while (filledcharcount <= fill_width + 2) {
            cout << ".";
            filledcharcount++;
        }
    }
    // public so we can use it to check strlength in main
    string stringrep() const {
        string new_str = last + ", " + first;
        return new_str;
    }

  private:
    string first, last;

};

class labscores_t {
public:
    // labscores_t();
    void add_data(int n_labscore) {
        scores.push_back(n_labscore);
    }
    // for sorting the lab scores and computing the median and mean scores
    void set_stats() {
        // default is easy for ints
        sort(scores.begin(), scores.end());
        median = scores[(scores.size()-1) / 2];
        // sum(items) / n_items
        // note 0.0 to ensure doubles operation
        mean = (double) accumulate(scores.begin(), scores.end(), 0.0) / scores.size();
    }
    // for printing the lab scores and stats to stdout
    void print_labscores() {
        for (vector<int>::iterator it = scores.begin(); it != scores.end(); it++) {
            cout << setw(3) << *it;
        }
        cout << " :";
        // personally I would have thought to make this 1 wider for '100' being 3 chars
        // but who actually get's 100s in cs140 right?
        // (this is just the way the instructions have it)
        cout << setw(3) << median << " ";
        cout << fixed << setprecision(1) << mean;
    }

    friend class heap_t;

private:
    vector<int> scores;
    int median;
    double mean;
};

// "name: scores" used for the map
typedef pair<name_t, labscores_t> score_set;

class heap_t {
public:
    // heap_t();
    bool empty() const {
        if (heapitems.size() == 0) return true;
        else return false;
    }
    void add_data(map<name_t, labscores_t>::iterator n_item) {
        data_t new_item(n_item);
        heapitems.push_back(new_item);
    }
    void heapify() {
        // organize the heap!
        make_heap(heapitems.begin(), heapitems.end());
    }
    map<name_t, labscores_t>::iterator extract_top() {
        pop_heap(heapitems.begin(), heapitems.end());
        data_t popped_item = heapitems.back();
        heapitems.pop_back();
        return popped_item.dataitem;
    }

private:
    struct data_t {
        map<name_t, labscores_t>::iterator dataitem;
        data_t(map<name_t, labscores_t>::iterator mit_new) {
            dataitem = mit_new;
        }
        bool operator<(const data_t &other) const {
            // first compare by mean
            if (dataitem->second.mean < other.dataitem->second.mean) {
                return true;
            } else if (dataitem->second.mean == other.dataitem->second.mean) {
                // then check by median
                if (dataitem->second.median < other.dataitem->second.median) {
                    return true;
                } else if (dataitem->second.median == other.dataitem->second.median) {
                    // finally by name lexo order, but reverse because max heeeeep
                    // if (DEBUG) cout << "namesort! on " << dataitem->first.stringrep() << " vs " << other.dataitem->first.stringrep() << endl;
                    return !(dataitem->first < other.dataitem->first);
                }
            }
            return false;
        }
    };
    vector<data_t> heapitems;
};

int cli_fail() {
    cerr << "usage: labscores -byname|-byrank|-top10 datafile.txt\n";
    return 1;
}

int main(int argc, char const *argv[]) {
    uint max_name_length = 0;

    // arguments
    // [prog opt data]
    if (argc != 3) {
        return cli_fail();
    }
    string option = argv[1];
    string infilename = argv[2];
    if (
        option != "-byname" &&
        option != "-byrank" &&
        option != "-top10"
    ) {
        return cli_fail();
    }

    if (DEBUG) cerr << "opening file...\n";
    ifstream infile;
    infile.open(infilename);
    if (infile.fail()) {
        cerr << "Could not open file for reading.\n";
        return 2;
    }

    // I like my python dicts, so I can at least pretend
    map<name_t, labscores_t> score_dict;
    heap_t score_heap;

    string curline;
    if (DEBUG) cerr << "reading file...\n";
    while (getline(infile, curline)) {

        // construct name_t for human
        // humans only have two part names here
        string first, last;
        stringstream thisline(curline);

        thisline >> first;
        thisline >> last;
        name_t n_human(last, first);
        if (n_human.stringrep().length() > max_name_length) {
            max_name_length = n_human.stringrep().length();
        }
        map<name_t, labscores_t>::iterator it = score_dict.find(n_human);
        if (it == score_dict.end()) {
            // it's not already in the dict,
            int in_score;
            labscores_t scores;
            while (thisline >> in_score) {
                scores.add_data(in_score);
            }
            scores.set_stats();
            // craft a pair for the map
            score_set new_scores(n_human, scores);
            // insert pair and put iterator into the heap
            score_heap.add_data(score_dict.insert(new_scores).first);
        } else {
            // according to instructions, do nothing with new dupes
        }
    }
    if (DEBUG) cerr << "done reading file, got " << score_dict.size() << " records\n";
    if (DEBUG) cerr << "longest name: " << max_name_length << "\n";

    // alright, now let's print it all back out

    // in order of name lexo as '-byname'
    if (option == "-byname") {
        for (map<name_t, labscores_t>::iterator it = score_dict.begin(); it != score_dict.end(); ++it) {
            it->first.print_name(max_name_length);
            it->second.print_labscores();
            cout << "\n";
        }
    } else if (option == "-byrank") {
        score_heap.heapify();
        while (!score_heap.empty()) {
            map<name_t, labscores_t>::iterator scorepair = score_heap.extract_top();
            scorepair->first.print_name(max_name_length);
            scorepair->second.print_labscores();
            cout << "\n";
        }
    } else if (option == "-top10") {
        int i = 0;
        // same as byrank, but limited
        score_heap.heapify();
        while (!score_heap.empty() && i < 10) {
            map<name_t, labscores_t>::iterator scorepair = score_heap.extract_top();
            scorepair->first.print_name(max_name_length);
            scorepair->second.print_labscores();
            cout << "\n";
            i++;
        }
    }

    return 0;
}
