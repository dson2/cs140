#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

const string face[] = { "Ace", "2", "3", "4", "5", "6", "7",
                        "8", "9", "10", "Jack", "Queen", "King" }; 
const string suit[] = { "Clubs", "Diamonds", "Hearts", "Spades" };

string random_card(bool verbose=false) {
    string card;

    card = face[ rand()%13 ];
    card += " of ";
    card += suit[ rand()%4 ];

    if (verbose)
      cout << card << "\n";

    return card;
}

int main(int argc, char *argv[])
{
    bool verbose = false;
    int seedvalue = 0;

    for (int i=1; i<argc; i++) {
      string option = argv[i];
      if (option.compare(0,6,"-seed=") == 0) {
        seedvalue = atoi(&argv[i][6]);
      } else if (option.compare("-verbose") == 0) {
        verbose = true;
      } else 
        cout << "option " << argv[i] << " ignored\n";
    }

    srand(seedvalue);

    // declare a table called deck that keeps track of
    // card faces dealt for each suit -- initialize to 0
    
    int** deck = new int*[4];
    for(int i = 0; i < 4; i++)
        deck[i] = new int[13];

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 13; j++) {
            deck[i][j] = 0;
            //cout << "clear " << i << " " << j << endl;
        }
    }
    string delimiter = " of ";

    bool allseen = false;
    int suitmatch;
    while (!allseen) {
        string card = random_card(verbose);

        // reverse engineer card suit and face
        string card_face = card.substr(0, card.find(delimiter));
        //if (verbose) cout << face << endl;
        string card_suit = card.substr(card.find(delimiter) + delimiter.length(), card.length());
        //if (verbose) cout << suit << endl;

        // loop through known suits to see if it's a match
        int card_nsuit = -1;
        for (int suitnum = 0; suitnum < 4; suitnum++) {
            if (card_suit.compare(suit[suitnum]) == 0) {
                card_nsuit = suitnum;
            }
        }
        int card_nface = -1;
        for (int facenum = 0; facenum < 13; facenum++) {
            if (card_face.compare(face[facenum]) == 0) {
                card_nface = facenum;
            }
        }

        if (verbose) cout << card_nsuit << " " << card_nface << endl;

        deck[card_nsuit][card_nface]++;

        // break out of loop if stopping criteria met

        // for each suit
        for (int i = 0; i < 4; i++) {
            allseen = true;
            for (int j = 10; j < 13; j++) {
                if (deck[i][j] == 0) {
                    allseen = false;
                }
                //if (verbose) cout << "checking " << i << " " << j << ": " << deck[i][j] << endl;
            }
            if (allseen == true) {
                suitmatch = i;
                break;
            }
        }
    }

    // print formatted table contents to stdout

    // for each suit
    for (int i = 0; i < 4; i++) {
        
        cout << setw(9) << suit[i] << ":";

	// for each card
        for (int j = 0; j < 13; j++) {
            cout << setw(4) << deck[i][j];
        }

	// mark matched suit
        if (suitmatch == i) {
            cout << " **";
        }
        cout << endl;
    }

}

