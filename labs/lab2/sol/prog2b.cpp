#include <cstdlib>
#include <iostream>
#include <iomanip>

#include <list>

using namespace std;

bool verbose = false;

const string face[] = { "Ace", "2", "3", "4", "5", "6", "7",
                        "8", "9", "10", "Jack", "Queen", "King" }; 
const string suit[] = { "Clubs", "Diamonds", "Hearts", "Spades" };

string random_card(bool verbose=false) {
    string rcard;

    rcard = face[ rand()%13 ];
    rcard += " of ";
    rcard += suit[ rand()%4 ];

    if (verbose)
      cout << rcard << "\n";

    return rcard;
}

// stores a card object, much easier than using those old arrays to get the human-readable name
struct card {
    int u_face, u_suit;

    card();
    card(int nface, int nsuit) {
        u_face = nface;
        u_suit = nsuit;
    }

    // comparison between cards critial for replacement
    inline bool operator==(card comp_card) {
        if (comp_card.u_face == u_face && comp_card.u_suit == u_suit) return true;
        else return false;
    }

    string stringrep() {
        string u_card;
        u_card = face[u_face];
        u_card += " of ";
        u_card += suit[u_suit];
        return u_card;
    }
    // used for getting human readable name
    string facename() {
        return face[u_face];
    }
    string suitname() {
        return suit[u_suit];
    }

};

// overload << operator for a card just to print the face
ostream& operator<< (ostream& outstream, card& incard) {
    outstream << incard.facename();
}

// overload for list<card>
ostream& operator<< (ostream& outstream, list<card>& incardlist) {
    // just loop each card as <<
    for (list<card>::iterator iT = incardlist.begin(); iT != incardlist.end(); ++iT) {
        outstream << " " << *iT;
    }
}

// take a card and put it into the list, moving duplicates
void place_card(list<card> &dest_list, card new_card) {
    list<card>::iterator iT;

    if (verbose) cout << "Placing card " << new_card.stringrep() << endl;

    // easier to just delete if there, then always place at front
    for (iT = dest_list.begin(); iT != dest_list.end(); ++iT) {
        if (*iT == new_card) {
            dest_list.erase(iT);
            if (verbose) cout << "Replacing old " << new_card.stringrep() << endl;
            break;
        }
    }
    dest_list.push_front(new_card);
}

int main(int argc, char *argv[])
{
    int seedvalue = 0;

    for (int i=1; i<argc; i++) {
      string option = argv[i];
      if (option.compare(0,6,"-seed=") == 0) {
        seedvalue = atoi(&argv[i][6]);
      } else if (option.compare("-verbose") == 0) {
        verbose = true;
      } else 
        cout << "option " << argv[i] << " ignored\n";
    }

    srand(seedvalue);

    // declare a table called deck that keeps track of
    // card faces dealt for each suit -- initialize to 0
    //
    // also now create a linked list for each suit
    
    int** deck = new int*[4];
    list<card>* card_stacks = new list<card>[4];

    for(int i = 0; i < 4; i++)
        deck[i] = new int[13];

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 13; j++) {
            deck[i][j] = 0;
            //cout << "clear " << i << " " << j << endl;
        }
    }
    string delimiter = " of ";

    bool allseen = false;
    int suitmatch;
    while (!allseen) {
        // icard -> incoming card
        string icard = random_card(verbose);

        // reverse engineer card suit and face
        string card_face = icard.substr(0, icard.find(delimiter));
        //if (verbose) cout << face << endl;
        string card_suit = icard.substr(icard.find(delimiter) + delimiter.length(), icard.length());
        //if (verbose) cout << suit << endl;

        // loop through known suits to see if it's a match
        int card_nsuit = -1;
        for (int suitnum = 0; suitnum < 4; suitnum++) {
            if (card_suit.compare(suit[suitnum]) == 0) {
                card_nsuit = suitnum;
            }
        }
        int card_nface = -1;
        for (int facenum = 0; facenum < 13; facenum++) {
            if (card_face.compare(face[facenum]) == 0) {
                card_nface = facenum;
            }
        }


        if (verbose) cout << card_nsuit << " " << card_nface << endl;

        deck[card_nsuit][card_nface]++;

        // if (verbose) cout << "placing new card" << endl;
        // create new card 
        card* new_card = new card(card_nface, card_nsuit);
        place_card(card_stacks[card_nsuit], *new_card);

        // break out of loop if stopping criteria met
        // for each suit
        for (int i = 0; i < 4; i++) {
            allseen = true;
            for (int j = 10; j < 13; j++) {
                if (deck[i][j] == 0) {
                    allseen = false;
                }
                //if (verbose) cout << "checking " << i << " " << j << ": " << deck[i][j] << endl;
            }
            if (allseen == true) {
                suitmatch = i;
                break;
            }
        }
    }

    // print formatted table contents to stdout

    // for each suit
    for (int i = 0; i < 4; i++) {
        // if (verbose) cout << "Suit " << i << " has " << card_stacks[i].size() << " cards" << endl;

        cout << setw(9) << suit[i] << ":";

        // for each card
        /* for (list<card>::iterator iT = card_stacks[i].begin(); iT != card_stacks[i].end(); ++iT) {
            card thiscard = *iT;
            // using the card struct to make printing easy
            cout << " " << thiscard;
        } */

        // Use a list output operator because instructions said so
        // hooplaha for abstraction!
        cout << card_stacks[i];

	    // mark matched suit
        if (suitmatch == i) {
            cout << " **";
        }
        cout << endl;
    }

}

