#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

// utility to make range comparison easier
// (this is in c++17 but not 11)
const int clamp(int in, int low, int high) {
    if (in < low) return low;
    else if (in > high) return high;
    else return in;
}

// copies a 9x9 to another.
const void copy_9(const int source[9][9], int dest[9][9]) {
    for (int i=0; i<9; i++) {
        copy(begin(source[i]), end(source[i]), begin(dest[i]));
    }
}

// utility struct for keeping track of seen/unique elements
struct uniq_9 {
    bool sidx[9];

    uniq_9() { for (int i=0; i<9; ++i) {sidx[i]=false;}}

    bool seen(int ni) {
        if (sidx[ni]) return true;
        else {
            sidx[ni] = true;
            return false;
        }
    }
};

struct uniq_9_z {
    bool sidx[9];
    uniq_9_z() { for (int i=0; i<9; ++i) {sidx[i]=false;}}
    bool seen(int ni) {
        //cerr << "checking if seen: " << ni << " ! " << endl;
        if (ni==0) return false;
        else {
            if (sidx[ni-1]) return true;
            else {
                sidx[ni-1] = true;
                return false;
            }
        }
    }
};

class sudoku {
    public:
        sudoku();

        void solve();
        bool is_solved();

        bool read(const char *);
        void write(const char *);
        void write(const char *, const char *);

    private:
        bool solve(int[9][9]);

        const bool is_solved(const int[9][9]);
        const bool has_blanks(const int[9][9]);
        const bool valid_values(const int[9][9]);
        const bool valid_uniques(const int[9][9]);
        bool valid_uniques_zex(int[9][9]);

        bool unsolved_valid_square(int, int, int);

        // various support functions
        // error_check_value()
        // error_check_uniqueness()

        void display();

        int game[9][9];
};

sudoku::sudoku() {
    for (int i=0; i<9; i++) {
        for (int j=0; j<9; j++)
            game[i][j] = 0;
    }
}

bool sudoku::unsolved_valid_square(int i, int j, int v) {
    if (
            i == clamp(i, 0, 8) && // i and j coords between 0-8
            j == clamp(j, 0, 8) &&
            v == clamp(v, 0, 9) ) // table values 0-9 unsolved
        return true;
    else return false;
}

bool sudoku::read(const char *fname) {
    cout << "READ\n";

    ifstream fin(fname);

    int i, j, v;
    bool success = true;

    while (fin >> i >> j >> v) {
        // error check grid indices
        if (!unsolved_valid_square(i, j, v) ) {
            // error!
            cerr << "ERROR while READING: ";
            cerr << "error on: " << i << " " << j << " " << v << endl;
            success = false;
        }
        if (success) game[i][j] = v;
    }

    fin.close();

    // exit if bad grid indices
    if (!success) cerr << "READ FAIL\n";
    return success;

    // error check data values
    // error check uniqueness
    // exit if errors detected
}

void sudoku::write(const char *fname) {
    ofstream fout(fname);

    for (int i=0; i<9; i++) {
        for (int j=0; j<9; j++) {
            if (0 < game[i][j]) {
                fout << i << " "
                    << j << " "
                    << game[i][j] << "\n";
            }
        }
    }

    fout.close();
}

void sudoku::write(const char *fname, const char *addon) {
    int N1 = strlen(fname);
    int N2 = strlen(addon);

    char *n_fname = new char[N1+N2+2];

    // strip .txt suffix, then concatenate _addon.txt
    strncpy(n_fname, fname, N1-4);
    strcpy(n_fname+N1-4, "_");
    strcpy(n_fname+N1-3, addon);
    strcpy(n_fname+N1-3+N2, ".txt");

    write(n_fname);

    delete [] n_fname;
}

void sudoku::display() {
    cout << "| ----------------------------- |\n";
    for (int i=0; i<9; i++) {
        for (int j=0; j<9; j++) {
            if (j%3 == 0)
                cout << "|";
            cout << "  " << game[i][j];
        }
        cout << "  |\n";
        if (i%3 == 2)
            cout << "| ----------------------------- |\n";
    }
}

void sudoku::solve() {
    cout << "SOLVE\n";

    int partial_solve[9][9];
    copy_9(game, partial_solve);
    // call recursive computation
    solve(partial_solve);

    display();

    // error check data values
    // error check uniqueness
    if (is_solved(partial_solve)) {
        copy_9(partial_solve, game);
    }
    // exit if errors detected
}

const bool sudoku::has_blanks(const int grid[9][9]) {
    for (int i=0; i<9; ++i) {
        for (int j=0; j<9; ++j) {
            if (grid[i][j] != clamp(grid[i][j], 1, 9))
                return true;
        }
    }
    return false;
}

/* valid_uniques( int[9][9] )
 * Checks to make sure that there is only ever one instance of each value
 * per column, row, block
 */
const bool sudoku::valid_uniques(const int grid[9][9]) {
    // check first dimension uniqueness
    for (int i=0; i<9; ++i) {
        uniq_9 sawn;
        for (int j=0; j<9; ++j) {
            // if we've already seen the value in this run before, it's no good
            if (sawn.seen(grid[i][j])) return false;
        }
    }

    // check second dimension uniqueness
    for (int i=0; i<9; ++i) {
        uniq_9 sawn;
        for (int j=0; j<9; ++j) {
            // if we've already seen the value in this run before, it's no good
            // notice the swapped i/j values
            if (sawn.seen(grid[j][i])) return false;
        }
    }

    // check square uniqueness
    // first dimension block
    for (int b_x=0; b_x<3; b_x++) {
        // second dimension block
        for (int b_y=0; b_y<3; b_y++) {
            uniq_9 sawn;
            // first dimension offset
            for (int o_x=0; o_x<3; o_x++) {
                // second dimension offset
                for (int o_y=0; o_y<3; o_y++) {
                    if (sawn.seen(grid[(b_x*3)+o_x][(b_y*3)+o_y])) return false;
                }
            }
        }
    }

    // otherwise, return true (no problems were found with it)
    return true;
}

/* valid_uniques( int[9][9] )
 * Same as non_zex, but this one allows zeroes to pass for unsolved grids
 */
bool sudoku::valid_uniques_zex(int grid[9][9]) {
    cerr << "valid_zex called" << endl;
    // check first dimension uniqueness
    for (int i=0; i<9; ++i) {
        uniq_9_z sawn;
        for (int j=0; j<9; ++j) {
            // if we've already seen the value in this run before, it's no good
            if (sawn.seen(grid[i][j])) return false;
        }
        // delete &sawn;
    }
    cerr << "first loop over" << endl;
    // check second dimension uniqueness
    for (int i=0; i<9; ++i) {
        uniq_9_z sawn;
        for (int j=0; j<9; ++j) {
            // if we've already seen the value in this run before, it's no good
            // notice the swapped i/j values
            if (sawn.seen(grid[j][i])) return false;
        }
    }

    // check square uniqueness
    // first dimension block
    for (int b_x=0; b_x<3; b_x++) {
        // second dimension block
        for (int b_y=0; b_y<3; b_y++) {
            uniq_9_z sawn;
            // first dimension offset
            for (int o_x=0; o_x<3; o_x++) {
                // second dimension offset
                for (int o_y=0; o_y<3; o_y++) {
                    if (sawn.seen(grid[(b_x*3)+o_x][(b_y*3)+o_y])) return false;
                }
            }
        }
    }

    // otherwise, return true (no problems were found with it)
    return true;
}

const bool sudoku::valid_values(const int grid[9][9]) {
    // sum the numbers in each row, col, block, to 45
    // first dimension
    int dim1_sum, dim2_sum;
    for (int i=0; i<9; ++i) {
        dim1_sum = 0;
        dim2_sum = 0;
        for (int j=0; j<9; ++j) {
            dim1_sum += grid[i][j];
            dim2_sum += grid[j][i];
        }
        if (dim1_sum != 45) return false;
        if (dim2_sum != 45) return false;
    }
    // blocks summation
    int block_sum;
    for (int b_x=0; b_x<3; b_x++) {
        // second dimension block
        for (int b_y=0; b_y<3; b_y++) {
            block_sum = 0;
            // first dimension offset
            for (int o_x=0; o_x<3; o_x++) {
                // second dimension offset
                for (int o_y=0; o_y<3; o_y++) {
                    block_sum += grid[(b_x*3)+o_x][(b_y*3)+o_y];
                }
            }
            if (block_sum != 45) return false;
        }
    }
    // else
    return true;
}

bool sudoku::is_solved() {
    return is_solved(game);
}

const bool sudoku::is_solved(const int potential_solution[9][9]) {
    // check both error functions
    if (
            !has_blanks(potential_solution) &&
            valid_values(potential_solution) &&
            valid_uniques(potential_solution)
       ) {
        return true;
    } else {
        return false;
    }
}

bool sudoku::solve(int partial_solve[9][9]) {
    // if solution found,
    if (is_solved(partial_solve)) {
        // return solution-found
        return true;
    }
    // set cell index (i,j)
    // there has to be a zero somewhere in here
    int dim1_s = -1, dim2_s = -1;
    for (int i=0; i<9; ++i) {
        for (int j=0; j<9; ++j) {
            if (partial_solve[i][j] == 0) {
                dim1_s = i;
                dim2_s = j;
                // in order to break out of two for loops:
                goto zerofind_loopend;
            }
        }
    }
zerofind_loopend:
    // we will be working with dim1_s and dim2_s now

    // determine valid values
    vector<int> x_finds;
    for (int x=1; x<=9; ++x) {
        partial_solve[dim1_s][dim2_s] = x;
        if (valid_uniques_zex(partial_solve)) {
            x_finds.push_back(x);
            break;
        }
    }
    partial_solve[dim1_s][dim2_s] = 0;
    // if no valid values left,
    if (x_finds.size() == 0) {
        // return road-to-nowhere
        // put back unknown (as originally given to us)
        partial_solve[dim1_s][dim2_s] = 0;
        // show that we didn't find a valid path this way:
        return false;
    }

    // iterate thru valid values
    for (uint v=0; v<x_finds.size(); v++) {
        // game[i][j] = next value
        partial_solve[dim1_s][dim2_s] = x_finds.at(v);
        // if solve(arguments) == solution-found
        if (solve(partial_solve)) {
            // return solution-found
            // don't modify the partial_solve on our way back up
            return true;
        }
    }

    // if you're here, we didn't find a solution on this branch
    // reset: game[i][j] = 0
    partial_solve[dim1_s][dim2_s] = 0;
    // return road-to-nowhere
    return false;
}

int main(int argc, char *argv[]) {
    srand(time(NULL));

    if ((argc != 3) ||
            (strcmp(argv[1], "-s") != 0) ||
            strstr(argv[argc-1], ".txt") == NULL) {
        cerr << "usage: Sudoku -s game.txt\n";
        exit(0);
    }

    sudoku sudoku_game;

    if (strcmp(argv[1], "-s") == 0) {
        if (!sudoku_game.read(argv[2])) return 1;
        sudoku_game.solve();
        if (sudoku_game.is_solved())
            sudoku_game.write(argv[2], "solved");
    }
}
