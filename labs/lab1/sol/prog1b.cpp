
#include <iostream>
#include <vector>

using namespace std;

int main() {
    int incoming_int, totalcount = 0;
    vector<int> intvect;

    while (cin >> incoming_int) {
        intvect.push_back(incoming_int);
        totalcount += 1;
    }

    cout << "N   = " << totalcount << endl;

    int sum = 0, min, max;

    min = intvect[0];
    max = intvect[0];

    for (int i = 0; i < totalcount; i++ ) {
        if (intvect[i] > max) {
            max = intvect[i];
        }
        else if (intvect[i] < min) {
            min = intvect[i];
        }
        sum += intvect[i];
    }

    cout << "sum = " << sum << endl;
    cout << "min = " << min << endl;
    cout << "max = " << max << endl;
}

