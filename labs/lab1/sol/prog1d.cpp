#include <iostream>
#include <vector>
#include <ctype.h>

using namespace std;

int action; // set to character offset to be performed

int checkargs(int argc, char* argv[]) {
    // check args and return nonzero if invalid
    if (argc != 3) {
        cerr << "you provided " << argc << " arguments, ";
        cerr << "program requires 2 arguments, -[de|en]code <0-9>" << endl;
        return 1;
    }

    string operation = argv[1];

    // check that first letter is a digit
    if (! isdigit(argv[2][0])) return 1;

    action = atoi(argv[2]);

    if (operation == "-decode") {
	// just encode in reverse
        action = -action;
    } else if (operation == "-encode") {
        // nothing needed to do here
    } else {
        cerr << "could not determine action: " << argv[1] << endl;
        return 1;
    }

    return 0;
}

char wrapchar(int number, int lowerbound, int upperbound) {
    // wrap a number to keep it in range of the bounds, looping from begin to end
    // basically, set lowerbound to zero, wrap around max, then put back offset
    number = number - lowerbound;
    //cerr << "offset char: " << number << " " << endl;
    int range = upperbound - lowerbound;
    number = (number + range) % range;
    number = number + lowerbound;
    return char(number);
}

string doaction(string inputline) {
    // perform the action on the input string, return new string
    string outstring = inputline;
    
    for (int i = 0; i < inputline.size(); i++) {
        // for each alpha character in the string
        if (isalpha(inputline[i]) ) {
            //cerr << "translate: " << inputline[i] << int(inputline[i]) << endl;
            if (isupper(inputline[i])) {
                // uppercase move (65-90 char range)
                outstring[i] = wrapchar(int(inputline[i]) + action, 65, 91);
            }
            else if (islower(inputline[i]) ) {
                // lowercase move (97-122 char range)
                outstring[i] = wrapchar(int(inputline[i]) + action, 97, 123);
            }
        }

        // terminal colors for debugging
        //cerr << "\033[33m" << outstring << "\033[0m" << endl;
    }

    return outstring;
}

int main(int argc, char* argv[]) {
    if (checkargs(argc, argv)) {
        cerr << "usage: prog1d -[de|en]code <0-9>" << endl;
        return 1;
    }
    //cerr << "action: " << action << endl;

    string line;

    while (getline(cin, line)) {
        cout << doaction(line) << endl;
    }

    return 0;
}
