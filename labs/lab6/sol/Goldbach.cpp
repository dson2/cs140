#include <iostream>
#include <vector>
#include <tuple>

using namespace std;

typedef enum { FIRST_SOLUTION, ALL_SOLUTIONS } program_mode_t;

// operator for vector<int>
ostream& operator<< (ostream& outstream, vector<int>& intvect) {
    outstream << "vector<int>[";
    outstream << intvect[0];
    for (uint x = 1; x != intvect.size(); ++x) {
        outstream << "," << intvect[x];
    }
    outstream << "]";
    return outstream;
}

class prime_partition {
  public:
    // constructor
    prime_partition(program_mode_t nmode) {
        mode = nmode;
        if (plist.size() == 0) plist.push_back(3);
    }
    // function operator
    void operator() (const int tnum) {
        if (tnum < 4) {
            cerr << "that's not a valid number for goldbach's conjecture\n";
            return;
        }
        expand_plist(tnum);
        cout << plist << "\n";

        // prime_sums those_primes;
        // those_primes = recurse_goldbach(those_primes);
        if (mode == FIRST_SOLUTION) {
            cout << prime_summations[0] << endl;
        } else {
            for (vector<vector<int>>::iterator it = prime_summations.begin(); it != prime_summations.end(); it++) {
                cout << *it << endl;
            }
        }
    }

  private:
    // miscl member functions
    void expand_plist(int highbound) {
        int i = plist.back();
        while (plist.back() < highbound) {
            i += 2;
        // for (int i=(plist.back()+2); i<(highbound + plist.back()); i+=2) {
            for (int j=2; j*j<=i; j++) {
                if (i % j == 0) break;
                else if ((j+1)*(j+1) > i) {
                    // cout << "pushing " << i << "\n";
                    plist.push_back(i);
                }
            }
        }
    }

    vector<vector<int>> goldbach_even(int target) {
        vector<vector<int>> sets;
        for (vector<int>::iterator pit_1 = plist.begin(); *pit_1*2 < target; pit_1++) {
            for (vector<int>::iterator pit_2 = plist.begin(); *pit_2*2<target; pit_2++) {
                if (*pit_1 + *pit_2 == target) {
                    vector<int> set2;
                    set2.push_back(*pit_1);
                    set2.push_back(*pit_2);
                    sets.push_back(set2);
                    if (mode == FIRST_SOLUTION) {
                        goto goldbach_even_exit;
                    }
                }
            }
        }

      goldbach_even_exit:
        return sets;
    }

    program_mode_t mode;

    // miscl member data
    vector<int> plist;
    vector<vector<int>> prime_summations;
};

int main(int argc, char *argv[])
{
    // commandline argument parsing
    std::string arg1;
    if (argc != 2) {
        cerr << "usage: " << argv[0] << " [-first|-all]" << endl;
        if (argc == 2 && ((arg1 != "-first" && arg1 != "-all"))) {
            arg1 = argv[1];
            cerr << "invalid arguments " << arg1 << endl;
        }
        return 1;
    }

    program_mode_t mode;

    // set mode
    if (arg1 == "-first") mode = FIRST_SOLUTION;
    else if (arg1 == "-all") mode = ALL_SOLUTIONS;

    prime_partition goldbach(mode);

    int number;
    while (1) {
        cout << "number = ";
        cin >> number;
        if (cin.eof())
        break;
        goldbach(number);
    }

    cout << "\n";
}
