#ifndef BST_H
#define BST_H

#include <iomanip>
#include <iostream>
#include <queue>
#include <vector>
using namespace std;

template <class TKey>
class bst {
  struct node {
    node(ID = default value);

	void print();

    TKey key;
	ID parameter

	parent info
    node *link[2];
  };
  
  public:
	class iterator {
	  public:
		default constructor (no argument)
		overloaded operators (++, *, ==, !=)
	  private:
	    friend class bst<TKey>;
		constructor (with argument)

	    node *p;
	};

	iterator begin() { ... }
	iterator end() { ... }

  public:
    bst() { Troot=NULL; }
	~bst() { clear(Troot); }

	bool empty() { return Troot==NULL; }

	void insert(TKey &);

	iterator lower_bound(const TKey &);
	iterator upper_bound(const TKey &);

	void print_bylevel();

  private:
	void clear(node *);
	node *insert(node *, TKey &);

	ID parameter
    node *Troot;
};

bst<TKey>::node constructor goes here

template <class TKey>
void bst<TKey>::node::print()
{
  cout << setw(3) << key << " :";

  output node and parent ID information
  change below to output subtree ID information

  if (link[0]) cout << " L=" << setw(3) << link[0]->key;
  else         cout << "      ";
  if (link[1]) cout << " R=" << setw(3) << link[1]->key;
  else         cout << "      ";

  cout << "\n";
}

bst<TKey>::iterator functions not defined above go here

template <class TKey>
void bst<TKey>::clear(node *T)
{
  if (T) {
    clear(T->link[0]);
    clear(T->link[1]);
    delete T;
    T = NULL;
  }
}

template <class TKey>
void bst<TKey>::insert(TKey &key)
{ 
  Troot = insert(Troot, key);
}

template <class TKey>
class bst<TKey>::node *bst<TKey>::insert(node *T, TKey &key)
{
  if (T == NULL) {
    update and set node ID 
    T = new node;
    T->key = key;
  } else if (T->key == key) {
    ;
  } else {
    set parent link 
    int dir = T->key < key;
    T->link[dir] = insert(T->link[dir], key);
  }

  return T;
}

bst<TKey>::lower_bound function goes here

bst<TKey>::upper_bound function goes here

template <class TKey>
void bst<TKey>::print_bylevel()
{
  if (Troot == NULL)
    return;

  queue<node *> Q;
  node *T;

  Q.push(Troot);
  while (!Q.empty()) {
    T = Q.front();
    Q.pop();

    T->print();
    if (T->link[0]) Q.push(T->link[0]);
    if (T->link[1]) Q.push(T->link[1]);
  }
}
#endif
